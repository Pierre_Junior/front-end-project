﻿using Application.Exceptions.Books;
using Application.Exceptions.Products;
using Application.Interfaces.Services;
using Domain.Entities;
using Domain.Entities.Member;
using Domain.Repositories;
using Newtonsoft.Json;
using Persistence;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Product = Domain.Entities.Product;

namespace Infrastructure.Repositories.Utilisateur
{
    public class ProductRepository : IProductRepository
    {
        private readonly HttpClient _httpClient;
        private readonly IHttpContextUserService _httpContextUserService;


        public ProductRepository(IHttpClientFactory httpClientFactory, IHttpContextUserService httpContextUserService) {

            _httpClient = httpClientFactory.CreateClient(HttpClientConfiguration.Name);
            _httpContextUserService= httpContextUserService;

        }
        public async Task CreateProduct(Product product)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _httpContextUserService.AuthToken);
            HttpResponseMessage response = await _httpClient.PostAsJsonAsync("product", product);
            if (!response.IsSuccessStatusCode)
            {
                CheckRefuseCase(response);
            }
        }

        public async Task DeleteProductWithId(string id)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _httpContextUserService.AuthToken);
            HttpResponseMessage response = await _httpClient.DeleteAsync($"product/{id}");
            
            if (!response.IsSuccessStatusCode)
            {
                CheckRefuseCase(response);
            }
        }

        public async  Task<Product?> FindById(string id)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _httpContextUserService.AuthToken);
            HttpResponseMessage response = await _httpClient.GetAsync($"product/{id}");
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Product>(responseContent);
            }
            else
            {
                CheckRefuseCase(response);
                return null;
            }
        }

        public async Task<List<Product>?> GetAll()
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _httpContextUserService.AuthToken);
            HttpResponseMessage response = await _httpClient.GetAsync("product/all");
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<Product>>(responseContent);
            }
            else
            {
                CheckRefuseCase(response);
                return null;
            }
        }

        public async Task UpdateProduct(Product product)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _httpContextUserService.AuthToken);
            HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"product/{product.Id}",product);
            if (!response.IsSuccessStatusCode)
            {
                CheckRefuseCase(response);
            }
        }

        private void CheckRefuseCase(HttpResponseMessage response)
        {
            if(response.StatusCode==HttpStatusCode.Unauthorized) throw new UnauthorizedAccessException(response.StatusCode.ToString());
            if (response.StatusCode == HttpStatusCode.NotFound) throw new ProductNotFoundException("Le produit n'a pas été trouvé");
            if (response.StatusCode == HttpStatusCode.Forbidden) throw new ProductAccessException("Vous ne pouvez pas modifier le produit");
            throw new Exception(response.StatusCode.ToString());

        }
    }
}
