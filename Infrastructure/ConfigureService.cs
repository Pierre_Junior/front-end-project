﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;

using Microsoft.AspNetCore.Http.Features;
using System;
using Microsoft.AspNetCore.Identity;
using Infrastructure.Repositories.Utilisateur;
using Domain.Repositories;
using Application.Interfaces.Services;
using Infrastructure.Services;

namespace Infrastructure;

public static class ConfigureServices
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        ConfigureInfrastructureServices(services);

        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());


        ConfigureAuthentication(services);

        return services;
    }

    private static void ConfigureInfrastructureServices(IServiceCollection services)
    {
        services.AddSingleton<IHttpContextUserService, HttpContextUserService>();
        services.AddScoped<IProductRepository, ProductRepository>();

    }



    private static void ConfigureAuthentication(IServiceCollection services)
    {
        //services.AddIdentityCore<User>(options =>
        //{
        //    options.Stores.MaxLengthForKeys = 128;
        //    options.User.RequireUniqueEmail = true;
        //    options.SignIn.RequireConfirmedEmail = true;
        //    options.Password.RequiredLength = 10;
        //    options.Password.RequireUppercase = true;
        //    options.Password.RequireLowercase = true;
        //    options.Password.RequireDigit = true;
        //    options.Password.RequireNonAlphanumeric = true;
        //    options.Password.RequiredUniqueChars = 6;
        //})
        //.AddRoles<Role>()
        //    .AddRoleManager<RoleManager<Role>>()
        //    .AddDefaultTokenProviders()
        //    .AddEntityFrameworkStores<JardinsEntenteDbContext>()
        //    .AddSignInManager<SignInManager<User>>();

        //// Add and configure Argon2 password hasher
        //services.AddScoped<IPasswordHasher<User>, Argon2PasswordHasher<User>>();
        //services.Configure<Argon2PasswordHasherOptions>(options =>
        //{
        //    options.Strength = Argon2HashStrength.Interactive;
        //});

        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme,
                options => { options.LoginPath = "/authentication/login"; })
            .AddCookie(IdentityConstants.TwoFactorRememberMeScheme, o =>
            {
                o.Cookie.Name = IdentityConstants.TwoFactorRememberMeScheme;
                o.ExpireTimeSpan = TimeSpan.FromMinutes(5);
            })
            .AddCookie(IdentityConstants.TwoFactorUserIdScheme, o =>
            {
                o.Cookie.Name = IdentityConstants.TwoFactorUserIdScheme;
                o.ExpireTimeSpan = TimeSpan.FromMinutes(5);
            })
            .AddCookie(IdentityConstants.ApplicationScheme, o =>
            {
                o.Cookie.Name = IdentityConstants.ApplicationScheme;
                o.ExpireTimeSpan = TimeSpan.FromMinutes(5);
            });
    }
}