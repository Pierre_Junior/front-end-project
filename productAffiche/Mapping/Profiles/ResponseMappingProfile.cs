﻿using Application.Common;
using AutoMapper;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using productAffiche.Features.Common;
using productAffiche.Features.Products;


namespace productAffiche.Mapping.Profiles;

public class ResponseMappingProfile : Profile
{
    public ResponseMappingProfile()
    {

        CreateMap<IdentityResult, SucceededOrNotResponse>();

        CreateMap<IdentityError, Error>()
            .ForMember(error => error.ErrorType, opt => opt.MapFrom(identity => identity.Code))
            .ForMember(error => error.ErrorMessage, opt => opt.MapFrom(identity => identity.Description));

        CreateMap<Product, ProductDto>();


    }

}