﻿using AutoMapper;
using productAffiche.Features.Products.EditProduct;
using Web.Features.Regles.CreateRegle;
using Product = Domain.Entities.Product;

namespace productAffiche.Mapping.Profiles;

public class RequestMappingProfile : Profile
{
    public RequestMappingProfile()
    {


        CreateMap<EditProductRequest, Product>();
        CreateMap<CreateProductRequest, Product>();
    }
}