namespace productAffiche.Features.Common;

public interface ISanitizable
{
    void Sanitize();
}