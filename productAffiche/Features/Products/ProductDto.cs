﻿namespace productAffiche.Features.Products
{
    public class ProductDto
    {
        public string Id { get; set; } = default!;
        public string Name { get; set; } = default!;
        public int Quantite { get; set; } = default!;
        public double Unit_Price { get; set; } = default!;
    }
}
