﻿using Domain.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;
using FastEndpoints;
using Domain.Entities;
using productAffiche.Features.Products;



namespace Web.Features.Regles.GetAllProduct
{
    public class GetAllProductsEndPoint: EndpointWithoutRequest<List<ProductDto>>
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;

        public GetAllProductsEndPoint(IMapper mapper, IProductRepository productRepository)
        {
            _mapper = mapper;
            _productRepository = productRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("products");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var products = await _productRepository.GetAll();
            await SendOkAsync(_mapper.Map<List<ProductDto>>(products), ct);
        }
    }
}
