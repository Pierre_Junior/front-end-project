﻿using FastEndpoints;
using FluentValidation;

namespace productAffiche.Features.Products.GetProduct
{
    public class GetProductValidator : Validator<GetProductRequest>
    {
        public GetProductValidator()
        {
            RuleFor(x => x.Id)
                .Matches(@"^[a-f\d]{24}$")
                .WithErrorCode("EmptyProductId")
                .WithMessage("Product id is required.");

        }

    }
}
