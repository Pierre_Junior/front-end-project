﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;


namespace productAffiche.Features.Products.GetProduct
{
    public class GetProductEndpoint: Endpoint<GetProductRequest, ProductDto>
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;

        public GetProductEndpoint(IMapper mapper, IProductRepository productRepository)
        {
            _mapper = mapper;
            _productRepository = productRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("products/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetProductRequest request, CancellationToken ct)
        {
            var product = await _productRepository.FindById(request.Id);
            await SendOkAsync(_mapper.Map<ProductDto>(product), ct);
        }
    }
}
