﻿namespace productAffiche.Features.Products.GetProduct
{
    public class GetProductRequest
    {
        public string Id { get; set; } = default!;

    }
}
