﻿namespace Web.Features.Regles.CreateRegle
{
    public class CreateProductRequest
    {
        public string Name { get; set; } = default!;
        public int Quantite { get; set; } = default!;
        public double Unit_Price { get; set; } = default!;
    }
}
