﻿using Application.Interfaces.Services.Products;
using Domain.Entities;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using productAffiche.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Regles.CreateRegle
{
    public class CreateProductEndPoint: Endpoint<CreateProductRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IProductCreationService _productRepository;


        public CreateProductEndPoint(IMapper mapper, IProductCreationService productRepository)
        {
            _mapper = mapper;
            _productRepository = productRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Post("products");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateProductRequest req, CancellationToken ct)
        {
            var request = _mapper.Map<Product>(req);

            await _productRepository.CreateProduct(request);

            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
