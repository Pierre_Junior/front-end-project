﻿using Application.Interfaces.Services.Products;
using Domain.Entities;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using productAffiche.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace productAffiche.Features.Products.EditProduct
{
    public class EditProductEndPoint: Endpoint<EditProductRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IProductEditService _productRepository;


        public EditProductEndPoint(IMapper mapper, IProductEditService productRepository)
        {
            _mapper = mapper;
            _productRepository = productRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Put("products/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(EditProductRequest req, CancellationToken ct)
        {
            var request = _mapper.Map<Product>(req);

            await _productRepository.UpdateRegle(request);

            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
