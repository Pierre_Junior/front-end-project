﻿namespace productAffiche.Features.Products.EditProduct
{
    public class EditProductRequest
    {
        public string Id { get; set; } = default!;
        public string Name { get; set; } = default!;
        public int Quantite { get; set; } = default!;
        public double Unit_Price { get; set; } = default!;
    }
}
