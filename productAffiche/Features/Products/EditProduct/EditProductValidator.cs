﻿using FastEndpoints;
using FluentValidation;
using Humanizer;

namespace productAffiche.Features.Products.EditProduct
{
    public class EditProductValidator : Validator<EditProductRequest>
    {
        const int MIN = 0;

        public EditProductValidator()
        {
            RuleFor(x => x.Id)
    .Matches(@"^[a-f\d]{24}$")
    .WithErrorCode("EmptyProductId")
    .WithMessage("Product id is required.");

            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidFrenchTitle")
                .WithMessage("Le titre de la regle ne peut pas être nulle ou vide");

            RuleFor(x => x.Quantite)
            .NotNull()
             .GreaterThanOrEqualTo(MIN)
    .WithErrorCode("InvalidX")
    .WithMessage($"La quantité doit être plus grand que {MIN}");

            RuleFor(x => x.Unit_Price)
    .NotNull()
    .GreaterThanOrEqualTo(MIN)
    .WithErrorCode("InvalidUnitPrice")
    .WithMessage($"Le prix unitaire doit être plus grand que {MIN} ");
        }
    }
}
