﻿using FastEndpoints;
using FluentValidation;

namespace productAffiche.Features.Products.DeleteProduct
{
    public class DeleteProductValidator: Validator<DeleteProductRequest>
    {
        public DeleteProductValidator()
        {
            RuleFor(x => x.Id)
                .Matches(@"^[a-f\d]{24}$")
                .WithErrorCode("EmptyProductId")
                .WithMessage("Product id is required.");
        }
    }
}
