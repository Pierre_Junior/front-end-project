﻿namespace productAffiche.Features.Products.DeleteProduct
{
    public class DeleteProductRequest
    {
        public string Id { get; set; } = default!;
    }
}
