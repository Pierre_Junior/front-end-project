﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace productAffiche.Features.Products.DeleteProduct
{
    public class DeleteProductEndPoint:Endpoint<DeleteProductRequest,EmptyResponse>
    {
        private readonly IProductRepository _productRepository;

        public DeleteProductEndPoint(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("products/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeleteProductRequest request, CancellationToken ct)
        {
            await _productRepository.DeleteProductWithId(request.Id);
            await SendNoContentAsync(ct);
        }
    }
}
