﻿using Application.Helpers;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using productAffiche.Cookies;
using productAffiche.Extensions;

namespace productAffiche.Controllers;

public class BaseController : Controller
{
    private readonly ILogger<BaseController> _logger;

    public BaseController(ILogger<BaseController> logger)
    {
        _logger = logger;
    }

    public override void OnActionExecuting(ActionExecutingContext context)
    {
        var cookieValue = HttpContext.GetCookieValue(CookieRequestCultureProvider.DefaultCookieName);
        _logger.Log(LogLevel.Information, $"Language value received from cookie : {cookieValue}");

        if (string.IsNullOrWhiteSpace(cookieValue))
        {
            CultureHelper.ChangeCurrentCultureTo(CultureHelper.DefaultTwoLetterLang);
            HttpContext.SetAspNetLanguageCookie(CultureHelper.DefaultTwoLetterLang);
            HttpContext.SetCookieValue(CookieNames.LANGUAGE_COOKIE_NAME, CultureHelper.DefaultTwoLetterLang, false, false);
        }
        else
        {
            var cultures = CookieRequestCultureProvider.ParseCookieValue(cookieValue);
            if (cultures != null && cultures.Cultures.Any() && cultures.UICultures.Any())
            {
                var twoLetterLang = cultures.Cultures.First().Value.Substring(0, 2);
                CultureHelper.ChangeCurrentCultureTo(twoLetterLang);
                HttpContext.SetCookieValue(CookieNames.LANGUAGE_COOKIE_NAME, twoLetterLang, false, false);
            }
        }

        base.OnActionExecuting(context);
    }
}