﻿using System.Security.Claims;
using System.Text.Json;
using Core.Flash;
using Domain.Entities.Member;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Persistence;
using productAffiche.ViewModels.Authentication;
using Serilog;

namespace productAffiche.Controllers;

public class AuthenticationController : BaseController
{
    private readonly IFlasher _flasher;
    private readonly IStringLocalizer<AuthenticationController> _localizer;
    private readonly HttpClient _httpClient;
    public AuthenticationController(
        ILogger<AuthenticationController> logger,
        IFlasher flasher,
        IStringLocalizer<AuthenticationController> localizer,
        IHttpClientFactory httpClientFactory) : base(logger)
    {
        _flasher = flasher;
        _localizer = localizer;
        _httpClient = httpClientFactory.CreateClient(HttpClientConfiguration.Name);
    }

    [HttpGet]
    [AllowAnonymous]
    public IActionResult Login(string returnUrl)
    {
        return View(new LoginViewModel { ReturnUrl = returnUrl });
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Login(LoginViewModel model)
    {
        if (!ModelState.IsValid)
            return View(model);
        
        model.Email = model.Email.Trim();

        HttpResponseMessage  response = await _httpClient.PostAsJsonAsync("auth/login",model);

        if(!response.IsSuccessStatusCode)
        {
            _flasher.Flash(Types.Warning, _localizer["InvalidUsernameOrPassword"], true);
            return View(model);
        }
        else
        {
            TokenAuth token =( await response.Content.ReadFromJsonAsync<TokenAuth>())!;
            return await AddClaimsAndSignInToHttpContext(token);
        }
    }

    private async Task<IActionResult> AddClaimsAndSignInToHttpContext(TokenAuth token)
    {
        var claims = new List<Claim>
        {
            new (ClaimTypes.Authentication, token.Token),
        };

        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
        await HttpContext.SignInAsync(new ClaimsPrincipal(claimsIdentity));

        return RedirectToAction("Index", "VueApp");
    }

    public async Task<IActionResult> Logout()
    {
        await HttpContext.SignOutAsync();

        return RedirectToAction("Login", "Authentication");
    }
}
