﻿using Application.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using productAffiche.Cookies;
using productAffiche.Extensions;

namespace productAffiche.Controllers.VueApp;

[Authorize]
[Route("/en")]
public class EnglishVueAppController : BaseController
{
    public EnglishVueAppController(ILogger<EnglishVueAppController> logger) : base(logger)
    {
    }

    [HttpGet]
    public IActionResult Index()
    {
        ChangeCultureToEnglish();
        ViewData["CurrentUrl"] = null;

        return View("VueAppPage");
    }

    [HttpGet]
    [Route("{**catchall}")]
    public IActionResult RedirectToSamePath()
    {
        ChangeCultureToEnglish();
        ViewData["CurrentUrl"] = Request.Path.Value;

        return View("VueAppPage");
    }

    private void ChangeCultureToEnglish()
    {
        CultureHelper.ChangeCurrentCultureTo("en");
        HttpContext.SetAspNetLanguageCookie("en");
        HttpContext.SetCookieValue(CookieNames.LANGUAGE_COOKIE_NAME, "en", false, false);
    }
}