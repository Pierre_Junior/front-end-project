﻿using Microsoft.AspNetCore.Mvc;
using productAffiche.Extensions;

namespace productAffiche.Controllers
{
    [Route("")]
    public class HomeController : BaseController
    {
        public HomeController(ILogger<HomeController> logger) : base(logger)
        {
        }

        // GET
        public IActionResult Index()
        {
            if (HttpContext.IsAuthenticated())
                return RedirectToAction("Index", "VueApp");

            return RedirectToAction("Login", "Authentication");
        }
    }
}
