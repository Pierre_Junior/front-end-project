//import { notify } from "@kyvg/vue3-notification";

//export function notifySuccess(text: string) {
//  notify({
//    text: text,
//    type: "success",
//  })
//}

//export function notifyError(text: string) {
//  notify({
//    text: text,
//    type: "error",
//  })
//}

import { toast } from 'vue3-toastify'
import 'vue3-toastify/dist/index.css'

export function notifySuccess(text: string) {
    toast.success(text, {
        autoClose: 3000,
        theme: 'colored'
    })
}

export function notifyError(text: string) {
    toast.error(text, {
        autoClose: 3000,
        theme: 'colored'
    })
}
