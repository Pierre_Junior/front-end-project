import { createRouter, createWebHistory } from "vue-router";
import i18n from "@/i18n";
import HomeView from "../views/HomeView.vue";
//import ProductEdit from "../views/ProductEdit.vue";

const router = createRouter({
    // eslint-disable-next-line
    scrollBehavior(to, from, savedPosition) {
        // always scroll to top
        return { top: 0 };
    },
    history: createWebHistory(),
    routes: [
        {
            path: i18n.t("routes.home.path"),
            name: i18n.t("routes.home.name"),
            component: HomeView,
        },
        {
            path: i18n.t("routes.produit.path"),
            name: "produit",
            component: () => import('../views/ProductEdit.vue'),
            props: true
        },
        {
            path: i18n.t("routes.create.path"),
            name: "creation",
            component: import('@/views/ProductAjout.vue'),
        },
        {
            path: '/:notFound(.*)',
            name: 'NotFound',
            component: () => import('../views/NotFound/NotFound.vue')
        },

    ]
});


export const Router = router;