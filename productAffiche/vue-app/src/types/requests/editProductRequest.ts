﻿export interface IEditProductRequest {
    id?: string
    name?: string
    quantite?: string
    unit_Price?: string
}