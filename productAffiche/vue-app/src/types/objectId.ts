export class ObjectId {

  public static isValid(str: string): boolean {
    const validRegex =
        /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
    return validRegex.test(str);
  }

}
