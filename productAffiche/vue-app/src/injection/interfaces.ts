// eslint-disable-next-line @typescript-eslint/no-empty-interface
import {
    ICreateProductRequest,
  IEditProductRequest
} from "@/types/requests"
import {SucceededOrNotResponse} from "@/types/responses"
import { product } from "@/types/entities"


export interface IApiService {
  headersWithJsonContentType(): any

  headersWithFormDataContentType(): any

  buildEmptyBody(): string
}

export interface IProductService {

    getAllProducts(): Promise<product[]>
    deleteProduct(productId: string): Promise<SucceededOrNotResponse>
    createProduct(request: ICreateProductRequest): Promise<SucceededOrNotResponse>
    editProduct(request: IEditProductRequest): Promise<SucceededOrNotResponse>
    getProduct(productId: string):Promise<product>
}