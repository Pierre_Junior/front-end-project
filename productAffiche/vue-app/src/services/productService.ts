import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { IProductService } from "../injection/interfaces";
import { product } from "../types";
import { ICreateProductRequest, IEditProductRequest } from "../types/requests";


@injectable()
export class productService extends ApiService implements IProductService {

    private checkRefuseCase(error: any) {
        if (error.response.data.errors[0].errorType == "UnauthorizedAccessException")
            window.location.replace("/Authentication/Logout/")
    }

    public async getAllProducts(): Promise<product[]> {
        console.log(process.env.VUE_APP_API_BASE_URL)
        const response = await this
            ._httpClient
            .get<AxiosResponse<product[]>>(`/api/products`)
            .catch( (error: AxiosError): AxiosResponse<product[]> => {
                this.checkRefuseCase(error) 
                return error.response as AxiosResponse<product[]>
            })
        return response.data as product[]
    }

    public async deleteProduct(productId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`/api/products/${productId}`)
            .catch( (error: AxiosError): AxiosResponse<SucceededOrNotResponse>=> {
                this.checkRefuseCase(error) 

                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }

    public async createProduct(request: ICreateProductRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateProductRequest, AxiosResponse<any>>(
                `/api/products`,
                request, this.headersWithJsonContentType())
            .catch( (error: AxiosError): AxiosResponse<SucceededOrNotResponse>=> {
                this.checkRefuseCase(error) 

                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async editProduct(request: IEditProductRequest): Promise<SucceededOrNotResponse> {

        const response = await this
            ._httpClient
            .put<IEditProductRequest, AxiosResponse<any>>(
                `/api/products/${request.id}`,
                request, this.headersWithJsonContentType())
            .catch( (error: AxiosError): AxiosResponse<SucceededOrNotResponse>=> {
                this.checkRefuseCase(error) 

                return error.response as AxiosResponse<SucceededOrNotResponse>
            })

        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
    public async getProduct(productId: string): Promise<product> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<product>>(`/api/products/${productId}`)
            .catch( (error: AxiosError): AxiosResponse<product> =>{
                this.checkRefuseCase(error) 

                return error.response as AxiosResponse<product>;
            });
        return response.data as product
    }


}