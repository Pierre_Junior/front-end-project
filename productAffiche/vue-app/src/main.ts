import { createApp } from "vue";
import App from "./App.vue";
import i18n from "@/i18n";
import Notifications from "@kyvg/vue3-notification";
import Vue3EasyDataTable from "vue3-easy-data-table";
import "vue3-easy-data-table/dist/style.css";
import VueTippy from 'vue-tippy'


import { Router } from "./router";

createApp(App)
    .use(i18n)
    .use(Router)
    .use(Notifications)
    .component('EasyDataTable', Vue3EasyDataTable)
    .use(VueTippy, {
        defaultProps: {
            offset: [0, 12],
            zIndex: 30000,
            placement: "bottom",
            theme: "custom-jardins-entente-app",
            interactive: true
        },
    })
    .mount("#app");
