import {Container} from "inversify";
import axios, {AxiosInstance} from 'axios';
import "reflect-metadata";

import {TYPES} from "@/injection/types";
import {
  IApiService,
  IProductService
} from "@/injection/interfaces";

import {
  ApiService,
  productService
   
} from "@/services";


const dependencyInjection = new Container();
dependencyInjection.bind<AxiosInstance>(TYPES.AxiosInstance).toConstantValue(axios.create())
dependencyInjection.bind<IApiService>(TYPES.IApiService).to(ApiService).inSingletonScope()
dependencyInjection.bind<IProductService>(TYPES.IProductService).to(productService).inSingletonScope()


function useProductService() {
    return dependencyInjection.get<IProductService>(TYPES.IProductService)
}

export {
  dependencyInjection,
  useProductService
};