﻿using System.ComponentModel.DataAnnotations;

namespace productAffiche.ViewModels.ForgotPassword;


public class ForgotPasswordViewModel
{
    [Required]
    public string UserName { get; set; } = default!;
}