﻿namespace Application.Interfaces.Services;

public interface IHttpContextUserService
{
    string? AuthToken { get; }
}