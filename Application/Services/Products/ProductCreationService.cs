﻿using Application.Interfaces.Services.Products;
using Domain.Entities;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Regle
{
    public class ProductCreationService : IProductCreationService
    {
        private readonly IProductRepository _productRepository;

        public ProductCreationService(
            IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task CreateProduct(Product product)
        {
            await _productRepository.CreateProduct(product);
        }

    }
}
