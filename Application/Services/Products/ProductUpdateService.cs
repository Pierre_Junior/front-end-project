﻿using Application.Interfaces.Services.Products;
using Domain.Entities;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Regle
{
    public class ProductUpdateService : IProductEditService
    {
        private readonly IProductRepository _productRepository;

        public ProductUpdateService(
            IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task UpdateRegle(Product product)
        {
            await _productRepository.UpdateProduct(product);
        }
    }
}
