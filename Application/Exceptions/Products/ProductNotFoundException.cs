﻿namespace Application.Exceptions.Products;

public class ProductNotFoundException: Exception
{
    public ProductNotFoundException(string message) : base(message) { }
}