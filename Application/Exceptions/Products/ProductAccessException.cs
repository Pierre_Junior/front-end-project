﻿namespace Application.Exceptions.Books;

public class ProductAccessException: Exception
{
    public ProductAccessException(string message) : base(message) { }
}