﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IProductRepository
    {
        Task<List<Product>?> GetAll();
        Task<Product?> FindById(string id);
        Task CreateProduct(Product product);
        Task UpdateProduct(Product product);
        Task DeleteProductWithId(string id);
    }
}
