﻿using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Product : ISanitizable
    {

        [JsonProperty("_id")]
        public string Id { get; private set; } = default!;

        [JsonProperty("name")]
        public string Name { get; private set; } = default!;

        [JsonProperty("quantite")]
        public int Quantite { get; private set; } = default!;

        [JsonProperty("unit_price")]
        public double Unit_Price {  get; private set; }=default!;

        public void SetName(string name)=>Name = name;
        public void SetQuantite(int quantite) => Quantite = quantite;
        public void SetUnitPrice(double unitPrice) => Unit_Price = unitPrice;

        public void SanitizeForSaving()
        {
            Name = Name.Trim();
        }
    }
}
