﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Member
{
    public class Member:ISanitizable
    {
        public string Email { get; private set; } = default!;
        public string Username { get; private set; } = default!;
        public string Password { get; private set; }= default!;

        public void SanitizeForSaving()
        {
            Username=Username.Trim();
            Email=Email.Trim();
        }
    }
}
