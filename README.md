# front-end project

## Stack

- .NET 8
- VUE JS

## Dependencies

- FastEndpoint

## Name
A front-end project based with .net and vuejs (SPA)

## Description
The project uses a combination of asp.net with cshtml language and uses the compiled vuejs files after the user log in.
This project does not have a specific database, because my challenge was to use 3 programming languages ​​to carry out the project:
    -Asp.net
    -vue js 
    -express js (for the api check this here https://gitlab.com/Pierre_Junior/api-projet)
The goal for me was to show ease in handling 3 programming languages. Even if we could have gone straight there with SSMS and ENTITY FRAMEWORK and thus divided my project into 5 layers (Application, domain, persistence, infrastructure and WEB). If this is the case for you, I would invite you to consult my project https://gitlab.com/deptinfo.cegepgarneau.ca/jardinsentente



## Installation
- Install latest SDK of .NET Core 8 [here] (https://dotnet.microsoft.com/en-us/download/dotnet/8.0)
- Restore nuget package
- Install nvm
- Install node 18.16.1
    ```bash
    $ nvm install 18.16.1
    $ nvm use 18.16.1
    ```

## Usage


### Build front-end Vue app
```bash

$ cd .\productAffiche\vue-app\
# Installs dependencies
$ npm install yarn
$ yarn install    

# Compiles and minifies for production
$ yarn build --watch
```

### Run front-end .NET Core 8 app
```bash
$ cd .\productAffiche
$ npm run dev

```

## Authors and acknowledgment
Pierre Junior (sibafwafpj@gmail.com)