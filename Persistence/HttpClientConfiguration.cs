﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
    public static class HttpClientConfiguration 
    {
        //private IConfiguration conf = default!;

        //public string Name => conf.GetConnectionString("Name")!;

        //public string BaseAddress => conf.GetConnectionString("BaseAddress")!;

        //public void Initialize(IConfiguration configuration)
        //{
        //    conf = configuration;
        //}
        public static string Name { get; private set; } =default!;

        public static string BaseAddress { get; private set; } =default!;

        public static void Initialize(IConfiguration configuration)
        {
            BaseAddress = configuration.GetConnectionString("BaseAddress")!;
            Name = configuration.GetConnectionString("Name")!;
        }
    }
}
