﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Persistence
{
    public static class ConfigureService
    {
        public static IServiceCollection AddPersistenceService(this IServiceCollection services,
      IConfiguration configuration)
        {
            ConfigurePersistence(configuration);
            ConfigureHttpClient(services, configuration);

            return services;
        }

        private static void ConfigurePersistence( IConfiguration configuration)
        {
            HttpClientConfiguration.Initialize(configuration);
        }

        private static void ConfigureHttpClient(IServiceCollection services, IConfiguration configuration)
        {
            // Register the IHttpClientFactory
            services.AddHttpClient();

            // configure default settings for HttpClient
            services.AddHttpClient(HttpClientConfiguration.Name,client =>
            {
                client.BaseAddress = new Uri($"{HttpClientConfiguration.BaseAddress}");
                client.DefaultRequestHeaders.Add("Accept", "application/json");
            });
        }

        public static void InitializeAndSeedDatabase(this IServiceProvider serviceProvider)
        {
           // using var scope = serviceProvider.CreateScope();
            //var initializer = scope.ServiceProvider.GetRequiredService<BdInitializer>();
            //await initializer.SeedAsync();
        }



    }
}
